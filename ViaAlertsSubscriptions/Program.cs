﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using SendGrid;
using SendGrid.Helpers.Mail;
using RestSharp;
using Newtonsoft.Json.Linq;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using System.Data.SqlClient;
using System.Configuration;
using System.Configuration.Assemblies;
using System.Data;
using System.IO;

//   ------ TODO ----------
// + integrate the transfers and make sure the boolean conversion isnt giving any errors 
// 
// + Current time is being catched now we need to test the other users times based on the info 
//

// + doing the logic in the foreach cause a mass spam of text - was able to figure out the issue and seperating the logic


namespace ViaAlertsSubscriptions
{
    class Program
    {
        public static SqlConnection objConn;
        static void Main(string[] args)
        {
            
            //grabbing this query will get you the valid users with subscriptions but need 
            //to alter the where clause for the time using the currentTime varaible
            
            string allUserQuery = " select a.*, b.*FROM Subscriptions a INNER JOIN Accounts b ON a.AccountID = b.AccountID";
            string connectionSql = ConfigurationManager.AppSettings["ConnString"];
            var date = DateTime.Now;
            var currentTime = date.Hour.ToString() + ":" + date.Minute.ToString();
            var currentHour = date.Hour.ToString();
            var addHour = date.Hour + 1;
            var hourAhead = addHour.ToString();
            var currentMin = date.Minute.ToString(); 
            DayOfWeek wk = DateTime.Today.DayOfWeek;
            string weekday = wk.ToString();
            string daySubColumn = weekday.Substring(0, 3);

            //use for testing (revert to 2, 3 users for testing ot remove the and to test all users)
            string testuserQuery = " select a.*, b.* FROM Subscriptions a " +
                "INNER JOIN Accounts b ON a.AccountID = b.AccountID " +
                "WHERE DATEPART(hh, FirstTripAlertFromTime)  >= 6 " +
                "AND DATEPART(hh, FirstTripAlertFromTime) <= 7 " +
                "AND " + daySubColumn + " = 1  AND SubscriptionID IN ( 11 ) ";

            //real world case use and what will be impllmented for the actual program 
            string userQuery = " select a.*, b.* FROM Subscriptions a " +
                "INNER JOIN Accounts b ON a.AccountID = b.AccountID " +
                "WHERE DATEPART(hh, FirstTripAlertFromTime) >= " + currentHour + "  " +
                "AND DATEPART(hh, FirstTripAlertFromTime) <= " + hourAhead+ "  " +
                "AND " + daySubColumn + " = 1; ";

            // time var check  
            Console.WriteLine("-----------------------------------------");
            string logPath = ConfigurationManager.AppSettings["Log"];
            Console.WriteLine(logPath);
            Console.WriteLine(currentTime);
            Console.WriteLine(currentHour);
            Console.WriteLine(hourAhead);
            Console.WriteLine(currentMin);
            Console.WriteLine(daySubColumn);
            Console.WriteLine("-----------------------------------------");
         
       // swa[ out 

            using (SqlConnection connection = new SqlConnection(connectionSql))
            {
                
                SqlCommand command = new SqlCommand(testuserQuery, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                // creating a list of user options makes it easier to know who we need to send alerts to, if they are
                // within the time frame they get put into the list 

                List<User> usersTimes = new List<User>();
                while (reader.Read())
                {
                    
                    //Console.WriteLine(String.Format("{0} \t | {1} \t | {2} \t | {3} \t| {4} \t {5} \t | {6} \t | {7} \t | {8} \t| {9} \t {10} \t | {11} \t | {12} \t | {13} \t| {14} \t {15} \t | {16} \t | {17} \t | {18} \t| {19} \t | {20} \t {21} \t | {22} \t | {23} \t | {24} \t",
                    //reader[0], reader[1], reader[2], reader[3], reader[4], reader[5], reader[6], reader[7], reader[8], reader[9], reader[10], reader[11], reader[12], reader[13], reader[14], reader[15], reader[16], reader[17], reader[18], reader[19], reader[20], reader[21], reader[22], reader[23], reader[24]));
                    //Console.WriteLine("-----------------------------------------");
                    int accountId = int.Parse(reader[1].ToString());
                    int subId = int.Parse(reader[0].ToString());
                    int routeId = int.Parse(reader[5].ToString());
                    string email = reader[25].ToString();
                    string phoneNumber = reader[27].ToString();
                    bool prefferedText = (bool)reader[32];
                    bool prefferedEmail = (bool) reader[33];
                   
                    //Each time it loops we pull the information read in the query and create a user as well as adding them to the list

                    usersTimes.Add(new User(accountId, subId, routeId, email, phoneNumber, prefferedText, prefferedEmail));

                    foreach (User user in usersTimes)
                    {   
                        Console.WriteLine(user.AccountId);
                        Console.WriteLine(user.Email);
                        Console.WriteLine(user.SubscriptionId);
                        Console.WriteLine("================");
                        var userRouteId = user.RouteId;
                        string alertsQuery = "SELECT DISTINCT a.*, b.AlertsID FROM Alerts a INNER JOIN AlertStops b ON a.AlertsID = b.AlertsID where routeid = " + userRouteId;
                        SqlCommand userCommand = new SqlCommand(alertsQuery, connection);
                        List<String> alertInfo = new List<String>();
                        string userEmail = user.Email;
                        SqlDataReader alertReader = userCommand.ExecuteReader();
                        
                        while (alertReader.Read())
                        {
                         
                        Console.WriteLine(String.Format("{0} \t | {1}| {2}| {3} ",
                        alertReader[0], alertReader[1], alertReader[2], alertReader[3]));

                            alertInfo.Add(alertReader[1].ToString());
                            alertInfo.Add(alertReader[3].ToString());
                            alertInfo.Add(alertReader[5].ToString());
                            alertInfo.Add(alertReader[13].ToString());

                           

                            string cause = alertReader[1].ToString();
                            string headerText = alertReader[3].ToString();
                            string description = alertReader[5].ToString();
                            string direction = alertReader[13].ToString();
                           
                            //Removed if else in statements for the moment untill we fix the iissue happening
                            // in the execute for the email. it works on its own but in the if else it throws a stackoverflow. 
                          
                            SMSNotifcation(description, user.PhoneNumber);
                           
                            Execute(headerText, cause, description, user.Email).Wait();


                        }

                    }

                }

            
            }

           // Used for orginal testing of the proto  but this chu
          //  testing out the functionality of pulling from db - this was from the proto testing phase with twillio

            string query = "SELECT * FROM dbo.Alerts WHERE AlertsID = 1519";
            //string connectionSql = "Server=DEVSVTESTSQL01;Database=VIAAlerts;User ID=Alerts;Password=Viadev@Alerts;Trusted_Connection=true";

            using (SqlConnection connection = new SqlConnection(connectionSql))
            {
                SqlCommand command = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                List<String> alertInfo2 = new List<String>();
                while (reader.Read())
                {

                    //Console.WriteLine(String.Format("{0} \t | {1} \t | {2} \t | {3} \t| {4} \t {5} \t | {6} \t | {7} \t | {8} \t| {9} \t",
                    //reader[0], reader[1], reader[2], reader[3], reader[4], reader[5], reader[6], reader[7], reader[8], reader[9]));
                    alertInfo2.Add(reader[1].ToString()); // Cause
                    alertInfo2.Add(reader[3].ToString()); // Header Text 
                    alertInfo2.Add(reader[5].ToString()); // DescText
                    alertInfo2.Add(reader[13].ToString()); // Dir name 

                }

                Console.WriteLine("--------------------------------------------------------");
                Console.WriteLine(alertInfo2[0]);
                Console.WriteLine(alertInfo2[1]);
                Console.WriteLine(alertInfo2[2]);
                Console.WriteLine(alertInfo2[3]);
                string cause = alertInfo2[0];
                string headerText = alertInfo2[1];
                string descText = alertInfo2[2];
                string dir = alertInfo2[3];

                //Execute(headerText, cause, descText, "eddy.bautista@viainfo.net").Wait();
               // SMSNotifcation(descText);

            }



        }
       



        // execute is a method out main method of emailRoute uses, this is the sendgrid method to prepare the email and 
        // the emailroute fills in the params, 

        static async Task Execute(string header, string cause, string alert, string userEmail)
        {
            // api key the twillio account gives you 
            var apikey = ConfigurationManager.AppSettings["EmailApiKey"];
            var client = new SendGridClient(apikey);
            var from = new EmailAddress("ViaDevTestEmail@Email.com");
            var to = new EmailAddress(userEmail);
            var subject = header + " due to " + cause;
            var plain = "for some reason it needs this to work";


            // Fill in info from api of the bus and the missing time
            var html = "<h1>" + alert + "</h1>";
            var msg = MailHelper.CreateSingleEmail(
                from,
                to,
                subject,
                plain,
                html

                );

            var response = await client.SendEmailAsync(msg);
        }


        //swiftly test file to get familiar  -- prediction time with sql tbls odometer dev
        public static void emailRoute()
        {
            var client = new RestClient("https://api.goswift.ly/real-time/san-antonio/predictions?stop=46637");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            string headerAuth = ConfigurationManager.AppSettings["HeaderAuthSwiftly"];
            request.AddHeader("authorization", headerAuth);
            IRestResponse response = client.Execute(request);
            var jsonString = response.Content;
            var x = JObject.Parse(jsonString);
            string email = "bautista.j.eddy@gmail.com";
            var headsign = x["data"]["predictionsData"][0]["destinations"][0]["headsign"].ToString();
            var routeName = x["data"]["predictionsData"][0]["routeName"].ToString();
            var estimatedTime = x["data"]["predictionsData"][0]["destinations"][0]["predictions"][0]["min"].ToString();
            Console.WriteLine(estimatedTime);
            if (Int32.Parse(estimatedTime) > 10)
            {
                Execute(routeName, headsign, estimatedTime, email).Wait();
            }
            else
            {
                Console.WriteLine("Bus on time");
            }
        }


        public static void SMSNotifcation(string alert, string number)
        {
            // Find your Account Sid and Token at twilio.com/console
            // DANGER! This is insecure. See http://twil.io/secure

            //twilio used for testing, to use comment out 52020 
            string accountSid = ConfigurationManager.AppSettings["TestAccountSid"];
            string authToken = ConfigurationManager.AppSettings["TestAuthToken"];
            TwilioClient.Init(accountSid, authToken);


            //52020 code 
            //const string accountSid = ConfigurationManager.AppSettings["AccountSid"];
            //const string apiKey = ConfigurationManager.AppSettings["APIKey"];
            //const string apiSecret = ConfigurationManager.AppSettings["APISecret"];
            //TwilioClient.Init(apiKey, apiSecret, accountSid);


            var message = MessageResource.Create(
                body: alert,
                // test twillio
                from: new Twilio.Types.PhoneNumber("+12512929423"),
                //52020 --uncomment to to use  
                //from: new Twilio.Types.PhoneNumber("+52020"),
                to: new Twilio.Types.PhoneNumber("+1" + number)
            );

            Console.WriteLine(message.Sid);
        }

    }
    
}
