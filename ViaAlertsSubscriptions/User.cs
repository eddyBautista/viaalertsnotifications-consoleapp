﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViaAlertsSubscriptions
{
    class User
    {
        private int accountId;
        private int subscriptionId;
        private int routeId;
        private string email;
        private string phoneNumber;
        private bool preferredText;
        private bool preferredEmail;
        

        public User(int accountId, int subId, int routeId, string email, string number, bool prefText, bool prefEmail)
        {
            this.accountId = accountId;
            this.subscriptionId = subId;
            this.routeId = routeId;
            this.email = email;
            this.phoneNumber = number;
            this.preferredText = prefText;
            this.preferredEmail = prefEmail;
            
        }
        
        public int AccountId
        {
            get { return accountId; }
            set { accountId = value; }
        }

        public int SubscriptionId
        {
            get { return subscriptionId; }
            set { subscriptionId = value; }
        }

        public int RouteId
        {
            get { return routeId; }
            set { routeId = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        public bool PreferredText
        {
            get { return PreferredText; }
            set { preferredText = value; }
        }

        public bool PreferredEmail
        {
            get { return preferredEmail; }
            set { preferredEmail = value; }
        }
    }
}
